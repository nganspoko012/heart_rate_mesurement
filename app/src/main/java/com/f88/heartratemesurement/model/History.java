package com.f88.heartratemesurement.model;

import com.f88.heartratemesurement.adapter.GsonUTCDateAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class History implements Comparable<History> {
    private long id;
    @SerializedName("user_id")
    private long userID;
    @JsonAdapter(GsonUTCDateAdapter.class)
    @SerializedName("created_at")
    private Date createdAt;
    private int value;
    private int duration;
    private String note;

    public History(long id, Date createdAt, long userID, int value, int duration, String note) {
        this.id = id;
        this.createdAt = createdAt;
        this.userID = userID;
        this.value = value;
        this.duration = duration;
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", userID=" + userID +
                ", createdAt=" + createdAt +
                ", value=" + value +
                ", duration=" + duration +
                ", note='" + note + '\'' +
                '}';
    }

    @Override
    public int compareTo(History another) {
        return this.createdAt.compareTo(another.createdAt);
    }
}
