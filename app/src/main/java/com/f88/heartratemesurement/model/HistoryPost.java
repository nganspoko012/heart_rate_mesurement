package com.f88.heartratemesurement.model;

import com.google.gson.annotations.SerializedName;

public class HistoryPost {
    @SerializedName("value")
    int value;
    @SerializedName("note")
    String note;

    public HistoryPost() {
        value = 0;
        note = "";
    }

    public HistoryPost(int value) {
        setValue(value);
        note = "";
    }

    public HistoryPost(int value, String note) {
        setValue(value);
        setNote(note);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = Math.max(value, 0);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        if (note != null)
            this.note = note;
    }
}
