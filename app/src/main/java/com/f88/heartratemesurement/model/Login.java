package com.f88.heartratemesurement.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Login {
    private long id;
    private String username;
    private String email;
    private List<History> results;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("refresh_token")
    private String refreshToken;

    public Login(long id, String username, String email, List<History> results, String accessToken, String refreshToken) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.results = results;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<History> getResults() {
        return results;
    }

    public void setResults(List<History> results) {
        this.results = results;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
