package com.f88.heartratemesurement.model;

import com.f88.heartratemesurement.adapter.GsonUTCDateAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class User {
    @SerializedName("id")
    private long id;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @JsonAdapter(GsonUTCDateAdapter.class)
    @SerializedName("created_at")
    private Date createdAt;
    @JsonAdapter(GsonUTCDateAdapter.class)
    @SerializedName("updated_at")
    private Date updatedAt;
    @SerializedName("results")
    private List<History> results;

    public User(long id, String username, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.username = username;
    }

    public User(long id, String username, String email, String password, Date createdAt, Date updatedAt, List<History> results) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.results = results;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", create_at=" + createdAt +
                ", update_at=" + updatedAt +
                ", results=" + results +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<History> getResults() {
        return results;
    }

    public void setResults(List<History> results) {
        this.results = results;
    }
}
