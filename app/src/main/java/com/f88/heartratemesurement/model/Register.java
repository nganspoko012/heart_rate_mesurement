package com.f88.heartratemesurement.model;

import java.util.List;

public class Register {
    private long id;
    private String username;
    private String email;
    private List<History> results;

    public Register(long id, String username, String email, List<History> results) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.results = results;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<History> getResults() {
        return results;
    }

    public void setResults(List<History> results) {
        this.results = results;
    }
}
