package com.f88.heartratemesurement.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.f88.heartratemesurement.BR;
import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.databinding.FragmentMeasurementBinding;
import com.f88.heartratemesurement.model.HistoryPost;
import com.f88.heartratemesurement.repository.ApiCaller;
import com.f88.heartratemesurement.utils.BPMMessage;
import com.f88.heartratemesurement.utils.CameraService;
import com.f88.heartratemesurement.utils.OutputAnalyzer;
import com.f88.heartratemesurement.viewmodel.HistoryViewModel;
import com.f88.heartratemesurement.viewmodel.MeasurementViewModel;
import com.f88.heartratemesurement.viewmodel.NoteViewModel;
import com.google.android.material.snackbar.Snackbar;


public class MeasurementFragment extends Fragment {

    public static final int MESSAGE_START_MEASURE = 1;
    public static final int MESSAGE_UPDATE_REALTIME = 2;
    public static final int MESSAGE_UPDATE_FINAL = 3;
    private static final int MENU_INDEX_EXPORT_RESULT = 1;
    private static final int MENU_INDEX_EXPORT_DETAILS = 2;
    private final int REQUEST_CODE_CAMERA = 0;
    private final boolean justShared = false;
    private FragmentMeasurementBinding binding;
    private MeasurementViewModel measurementVM;
    private HistoryViewModel historyVM;
    private CameraService cameraService;
    private OutputAnalyzer analyzer;
    private TextView statusTextView;
    @SuppressLint("HandlerLeak")
    private final Handler mainHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            if (msg.what == MESSAGE_START_MEASURE) {
                measurementVM.status.setValue("Measuring...");
                measurementVM.bpmResult.setValue(0);
            }

            if (msg.what == MESSAGE_UPDATE_REALTIME) {

                BPMMessage bpmMessage = msg.obj instanceof BPMMessage ? (BPMMessage) msg.obj : null;

                if (bpmMessage != null) {
                    measurementVM.currentProgress.setValue(bpmMessage.getProgress());
                    if (bpmMessage.getValue() == 0)
                        return;
                    measurementVM.bpmResult.setValue(bpmMessage.getValue());
                }
            }

            if (msg.what == MESSAGE_UPDATE_FINAL) {
                measurementVM.status.setValue("Finished");
                measurementVM.currentProgress.setValue(100);
                int bpmResult = (int) msg.obj;
                measurementVM.bpmResult.setValue(bpmResult);
                showAddNoteDialog();
                //Release the click to measure
                statusTextView.setClickable(true);
            }
        }
    };
    private NoteViewModel noteViewModel;

    private long userId;
    private String accessToken;
    private String refreshToken;

    @Override
    public void onResume() {
        super.onResume();
        statusTextView.setClickable(true);

//        analyzer = new OutputAnalyzer(getActivity(), requireView().findViewById(R.id.graphTextureView), mainHandler);
//
//        TextureView cameraTextureView = requireView().findViewById(R.id.textureView);
//        SurfaceTexture previewSurfaceTexture = cameraTextureView.getSurfaceTexture();
//
//        // justShared is set if one clicks the share button.
//        if ((previewSurfaceTexture != null) && !justShared) {
//            // this first appears when we close the application and switch back
//            // - TextureView isn't quite ready at the first onResume.
//            Surface previewSurface = new Surface(previewSurfaceTexture);
//
//            // show warning when there is no flash
//            if (!requireActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
//                Snackbar.make(requireActivity().findViewById(R.id.constraintLayout), getString(R.string.noFlashWarning), Snackbar.LENGTH_LONG);
//            }
//
//            cameraService.start(previewSurface);
//            analyzer.measurePulse(cameraTextureView, cameraService);
//        }
    }

    private void getLoggedUser() {
        SharedPreferences sharedPref = requireActivity().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        userId = sharedPref.getLong("id", 0);
        accessToken = sharedPref.getString("access_token", "");
        refreshToken = sharedPref.getString("refresh_token", "");
        Log.i(ApiCaller.LOG, userId + "\\" + accessToken + " \\ " + refreshToken);
    }


    @Override
    public void onPause() {
        super.onPause();
        cameraService.stop();
        if (analyzer != null)
            analyzer.stop();
        statusTextView.setClickable(true);
        analyzer = new OutputAnalyzer(getActivity(), requireView().findViewById(R.id.graphTextureView), mainHandler);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(requireActivity(),
                new String[]{Manifest.permission.CAMERA},
                REQUEST_CODE_CAMERA);

        getLoggedUser();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_measurement, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        measurementVM = new ViewModelProvider(requireActivity()).get(MeasurementViewModel.class);
        historyVM = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
        noteViewModel = new ViewModelProvider(requireActivity()).get(NoteViewModel.class);

        noteViewModel.note.observe(getViewLifecycleOwner(), note -> {
            if (note != null) {
                if (measurementVM.bpmResult.getValue() != null)
                    postHistoryToServer(measurementVM.bpmResult.getValue(), note);
            }
        });

        binding.setVariable(BR.measurementVM, measurementVM);
        binding.setVariable(BR.historyVM, historyVM);
        cameraService = new CameraService(this.requireActivity());
        statusTextView = requireActivity().findViewById(R.id.statusText);
        statusTextView.setOnClickListener(this::onClickNewMeasurement);

        historyVM.getHistoriesFromRepository(userId, accessToken);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_CAMERA) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Snackbar.make(
                        requireActivity().findViewById(R.id.constraintLayout),
                        getString(R.string.cameraPermissionRequired),
                        Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public void onClickNewMeasurement(View view) {
        //Lock the click will be release on handler
        view.setClickable(false);

        analyzer = new OutputAnalyzer(requireActivity(), requireView().findViewById(R.id.graphTextureView), mainHandler);
        // clear prior results
        char[] empty = new char[0];
        TextureView cameraTextureView = requireView().findViewById(R.id.textureView);
        SurfaceTexture previewSurfaceTexture = cameraTextureView.getSurfaceTexture();

        if (previewSurfaceTexture != null) {
            // this first appears when we close the application and switch back
            // - TextureView isn't quite ready at the first onResume.
            Surface previewSurface = new Surface(previewSurfaceTexture);
            cameraService.start(previewSurface);
            analyzer.measurePulse(cameraTextureView, cameraService);
        }
    }

    private void showAddNoteDialog() {
        DialogFragment dialogFragment = new AddNoteDialogFragment();
        dialogFragment.show(getParentFragmentManager(), "AddNoteDialogFragment");
    }

    private void postHistoryToServer(int bpmResult, String note) {
        historyVM.createHistory(userId, accessToken, new HistoryPost(
                bpmResult,
                note
        ));
    }


    public interface OnFragmentInteractionListener {
        void OnFragmentInteraction(Uri uri);
    }


}