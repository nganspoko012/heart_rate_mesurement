package com.f88.heartratemesurement.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.viewmodel.NoteViewModel;

import org.jetbrains.annotations.NotNull;

public class AddNoteDialogFragment extends DialogFragment {

    NoteViewModel noteViewModel;

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        LayoutInflater inflater = requireActivity().getLayoutInflater();
        noteViewModel = new ViewModelProvider(requireActivity()).get(NoteViewModel.class);

        builder.setView(inflater.inflate(R.layout.dialog_add_note_layout, null))
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText noteEditText = requireDialog().findViewById(R.id.noteEditText);
                        noteViewModel.note.setValue(noteEditText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        noteViewModel.note.setValue("");
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

}
