package com.f88.heartratemesurement.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.adapter.HistoryRecycleAdapter;
import com.f88.heartratemesurement.viewmodel.HistoryViewModel;

public class HistoryFragment extends Fragment {
    private static final String TAG = "History API";
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    HistoryRecycleAdapter adapter;
    private HistoryViewModel historyVM;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        historyVM = new ViewModelProvider(requireActivity()).get(HistoryViewModel.class);
        recyclerSetup(view);
        observerSetup();
    }

    private void observerSetup() {
        historyVM.getHistories().observe(getViewLifecycleOwner(),
                histories -> adapter.setHistoryList(histories));
    }

    public void recyclerSetup(View view) {
        recyclerView = view.findViewById(R.id.historyRecycle);
        layoutManager = new LinearLayoutManager(view.getContext());
        adapter = new HistoryRecycleAdapter(R.layout.history_item_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public interface OnFragmentInteractionListener {
        void OnFragmentInteraction(Uri uri);
    }


}