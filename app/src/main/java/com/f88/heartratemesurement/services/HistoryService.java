package com.f88.heartratemesurement.services;

import com.f88.heartratemesurement.model.History;
import com.f88.heartratemesurement.model.HistoryPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface HistoryService {
    @GET("/user/{id}/result/all")
    Call<ApiResponse<List<History>>> getHistories(@Path("id") long userId, @Header("x-access-token") String accessToken);

    @GET("/user/{userID}/result/{id}")
    Call<ApiResponse<History>> getHistory(@Path("userID") long userID, @Path("id") long historyID);

    @POST("/user/{userID}/result")
    Call<ApiResponse<History>> createHistory(@Path("userID") long userID, @Header("x-access-token") String accessToken, @Body HistoryPost historyPost);

    @PUT("/user/{userID}/result/{id}")
    Call<History> updateHistory(@Path("userID") long userID, @Path("id") long historyID, @Body HistoryPost historyPost);
}
