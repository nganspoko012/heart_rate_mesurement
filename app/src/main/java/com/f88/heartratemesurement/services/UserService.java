package com.f88.heartratemesurement.services;

import com.f88.heartratemesurement.model.Login;
import com.f88.heartratemesurement.model.LoginPost;
import com.f88.heartratemesurement.model.Register;
import com.f88.heartratemesurement.model.RegisterPost;
import com.f88.heartratemesurement.model.User;
import com.f88.heartratemesurement.model.UserPost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {
    @GET("/user/all")
    Call<ApiResponse<List<User>>> getUsers();

    @GET("/user/{id}")
    Call<ApiResponse<User>> getUserByID(@Header("x-access-token") String accessToken, @Path("id") long userID);

    @POST("/user")
    Call<ApiResponse<User>> createUser(@Body UserPost userPost);

    @PUT("/user/{id}")
    Call<ApiResponse<User>> updateUser(@Header("x-access-token") String accessToken, @Path("id") long userID, @Body User user);

    @DELETE("/user/{id}")
    Call<User> deleteUser(@Body User user);

    @POST("/login")
    Call<ApiResponse<Login>> login(@Body LoginPost loginPost);

    //Get refresh token from server
    //Response will be a string
    @POST("/renew-token")
    Call<ApiResponse<String>> getRefreshToken(@Header("x-access-token") String accessToken, @Header("x-refresh-token") String refreshToken);

    @POST("/register")
    Call<ApiResponse<Register>> register(@Body RegisterPost registerPost);


}
