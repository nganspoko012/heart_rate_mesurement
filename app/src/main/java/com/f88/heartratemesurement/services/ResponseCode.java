package com.f88.heartratemesurement.services;

public enum ResponseCode {
    SUCCESS(200),
    FAIL(401),
    SERVER_ERROR(503);

    private final int value;

    ResponseCode(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
