package com.f88.heartratemesurement.services;

import com.f88.heartratemesurement.adapter.GsonUTCDateAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    //Connect to localhost server
    private static final String BASE_URL = "http://10.0.2.2:8080";
    private static RetrofitClient instance;
    private final HistoryService historyService;
    private final UserService userService;

    private RetrofitClient() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new GsonUTCDateAdapter())
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        historyService = retrofit.create(HistoryService.class);
        userService = retrofit.create(UserService.class);
    }

    public static synchronized RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;
    }

    public HistoryService getHistoryService() {
        return historyService;
    }

    public UserService getUserService() {
        return userService;
    }

}
