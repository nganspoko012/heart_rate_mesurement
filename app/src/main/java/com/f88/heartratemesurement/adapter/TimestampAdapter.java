package com.f88.heartratemesurement.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.sql.Timestamp;

public class TimestampAdapter extends TypeAdapter<Timestamp> {

    @Override
    public Timestamp read(JsonReader in) throws IOException {
        return new Timestamp(in.nextLong());  // convert seconds to milliseconds
    }

    @Override
    public void write(JsonWriter out, Timestamp timestamp) throws IOException {
        out.value(timestamp.getTime());  // convert milliseconds to seconds
    }
}