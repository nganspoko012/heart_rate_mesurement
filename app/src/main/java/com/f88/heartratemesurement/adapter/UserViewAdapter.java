package com.f88.heartratemesurement.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.model.User;

import java.util.List;

public class UserViewAdapter extends RecyclerView.Adapter<UserViewAdapter.ViewHolder> {

    private List<User> userList;


    public void setUserList(List<User> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.userEmail.setText(String.valueOf(userList.get(position).getEmail()));
        holder.userPassword.setText(String.valueOf(userList.get(position).getPassword()));
    }

    @Override
    public int getItemCount() {
        return userList == null ? 0 : userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView userEmail;
        public final TextView userPassword;

        public ViewHolder(View view) {
            super(view);
            userEmail = view.findViewById(R.id.userEmail);
            userPassword = view.findViewById(R.id.userPassword);
        }
    }
}