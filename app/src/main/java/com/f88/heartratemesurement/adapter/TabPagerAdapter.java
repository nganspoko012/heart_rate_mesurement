package com.f88.heartratemesurement.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.f88.heartratemesurement.fragment.HelpFragment;
import com.f88.heartratemesurement.fragment.HistoryFragment;
import com.f88.heartratemesurement.fragment.MeasurementFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {

    int tabCount;

    public TabPagerAdapter(@NonNull FragmentManager fm, int numberOfTabs) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.tabCount = numberOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HelpFragment();
            case 1:
                return new MeasurementFragment();
            case 2:
                return new HistoryFragment();
//            case 3:
//                return new UserFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
