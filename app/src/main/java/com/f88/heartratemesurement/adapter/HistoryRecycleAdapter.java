package com.f88.heartratemesurement.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.model.History;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class HistoryRecycleAdapter extends RecyclerView.Adapter<HistoryRecycleAdapter.ViewHolder> {
    private final int historyItemLayout;
    private List<History> historyList;

    public HistoryRecycleAdapter(int layoutID) {
        historyItemLayout = layoutID;
    }

    public void setHistoryList(List<History> histories) {
        historyList = histories;
        Collections.sort(historyList, Collections.reverseOrder());
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HistoryRecycleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(historyItemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        holder.dateTextView.setText(simpleDateFormat.format(historyList.get(position).getCreatedAt()));
        holder.heartRateTV.setText(String.valueOf(historyList.get(position).getValue()));
    }

    @Override
    public int getItemCount() {
        return historyList == null ? 0 : historyList.size();
    }

    //ViewHolder class reference to history_item_view.xml
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView dateTextView;
        TextView heartRateTV;

        public ViewHolder(View itemView) {
            super(itemView);
            dateTextView = itemView.findViewById(R.id.historyDateTV);
            heartRateTV = itemView.findViewById(R.id.historyHeartRateTV);
        }
    }

}
