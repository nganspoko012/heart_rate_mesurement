package com.f88.heartratemesurement.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.databinding.ActivitySignupBinding;
import com.f88.heartratemesurement.model.Register;
import com.f88.heartratemesurement.viewmodel.SignUpViewModel;

public class SignUpActivity extends AppCompatActivity {
    private SignUpViewModel signUpVM;
    private ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signUpVM = new ViewModelProvider(this).get(SignUpViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        binding.setSignUpViewModel(signUpVM);
        setUpObserver();
    }

    //todo auto login when user registered

    private void setUpObserver() {
        signUpVM.registeredUser.observe(this, new Observer<Register>() {
            @Override
            public void onChanged(Register user) {
                if (user != null) {
                    saveLoggedUser();
                    toLogin();
                } else {
                    showError();
                }
            }
        });
    }

    private void saveLoggedUser() {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("id", signUpVM.registeredUser.getValue() == null ? 0 : signUpVM.registeredUser.getValue().getId());
        editor.putString("username", signUpVM.registeredUser.getValue().getUsername());
        editor.putString("email", signUpVM.registeredUser.getValue().getEmail());
        editor.putString("password", signUpVM.password.getValue());
        editor.apply();
    }


    private void showError() {
        Toast.makeText(this, "Sign up failed!", Toast.LENGTH_LONG).show();
    }


    private void toMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
//        Gson gson = new Gson();
//        String userGson = gson.toJson(signUpVM.registeredUser);
//        i.putExtra("LoggedUser", userGson);
        startActivity(i);
    }

    private void toLogin() {
        Intent i = new Intent(getApplication(), LoginActivity.class);
        startActivity(i);
    }

    public void toLogin(View view) {
        Intent i = new Intent(getApplication(), LoginActivity.class);
        startActivity(i);
    }

    public void registerUser(View view) {
        signUpVM.registerUser();
    }

}