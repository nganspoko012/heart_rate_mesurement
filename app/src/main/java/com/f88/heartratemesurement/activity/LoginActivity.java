package com.f88.heartratemesurement.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.databinding.ActivityLoginBinding;
import com.f88.heartratemesurement.model.Login;
import com.f88.heartratemesurement.model.LoginPost;
import com.f88.heartratemesurement.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    private LoginViewModel loginVM;

    // todo Load saved registered user

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginVM = new ViewModelProvider(this).get(LoginViewModel.class);
        binding.setLoginViewModel(loginVM);
        //Observer for Login feature when user complete form
        setUpObserver();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setUpObserver() {
        loginVM.loggedUser.observe(this, new Observer<Login>() {
            @Override
            public void onChanged(Login user) {
                if (user != null) {
                    saveLoggedUser();
                    toMainActivity();
                } else {
                    showError();
                }
            }
        });
    }

    private void getLoggedUser() {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        long id = sharedPref.getLong("id", 0);
        String username = sharedPref.getString("username", "");
        String email = sharedPref.getString("email", "");
        String accessToken = sharedPref.getString("access_token", "");
        String refreshToken = sharedPref.getString("refresh_token", "");

        if (!username.isEmpty() && !email.isEmpty() && !accessToken.isEmpty() && !refreshToken.isEmpty()) {
            loginVM.loggedUser.setValue(new Login(id, username, email, null, accessToken, refreshToken));
            toMainActivity();
        }

    }


    private void saveLoggedUser() {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("id", loginVM.loggedUser.getValue() == null ? 0 : loginVM.loggedUser.getValue().getId());
        editor.putString("username", loginVM.loggedUser.getValue().getUsername());
        editor.putString("password", loginVM.password.getValue());
        editor.putString("email", loginVM.loggedUser.getValue().getEmail());
        editor.putString("access_token", loginVM.loggedUser.getValue().getAccessToken());
        editor.putString("refresh_token", loginVM.loggedUser.getValue().getRefreshToken());
        editor.apply();
    }

    private void showError() {
        Log.i("LoginActivity", "username incorrect");
    }

    private void toMainActivity() {
        Log.i("LoginActivity", "To main");
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void toSignUp(View view) {
        Intent i = new Intent(getApplication(), SignUpActivity.class);
        startActivity(i);
    }

    private void login() {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String username = sharedPref.getString("username", "");
        String password = sharedPref.getString("email", "");
        if (!password.isEmpty()) {
            loginVM.login(new LoginPost(username, password));
        }
    }


    public void login(View view) {
        Log.i("Register", loginVM.email.getValue() + ", " + loginVM.password.getValue());
        loginVM.login(new LoginPost(loginVM.email.getValue(), loginVM.password.getValue()));
    }

}