package com.f88.heartratemesurement.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.f88.heartratemesurement.R;
import com.f88.heartratemesurement.adapter.TabPagerAdapter;
import com.f88.heartratemesurement.fragment.HistoryFragment;
import com.f88.heartratemesurement.fragment.MeasurementFragment;
import com.f88.heartratemesurement.fragment.UserFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity
        implements MeasurementFragment.OnFragmentInteractionListener,
        HistoryFragment.OnFragmentInteractionListener,
        UserFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        configureTabLayout();
    }

    private void configureTabLayout() {
        TabLayout tabLayout = createTabLayout();
        ViewPager viewPager = createViewPager(tabLayout);

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout)
        );
        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                }
        );
    }

    private TabLayout createTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.help));
        //Setup 3 tab needed for Proof of concept
        tabLayout.addTab(tabLayout.newTab().setText(R.string.measure));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.history));
//        tabLayout.addTab(tabLayout.newTab().setText("User"));
        return tabLayout;
    }

    private ViewPager createViewPager(TabLayout tabLayout) {
        final ViewPager pager = findViewById(R.id.pager);
        final TabPagerAdapter adapter = new TabPagerAdapter(getSupportFragmentManager(),
                tabLayout.getTabCount());
        pager.setAdapter(adapter);
        return pager;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout) {
            logoutUser();
            toLogin();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logoutUser() {
        SharedPreferences sharedPref = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    private void toLogin() {
        Intent i = new Intent(getApplication(), LoginActivity.class);
        startActivity(i);
    }

    @Override
    public void OnFragmentInteraction(Uri uri) {

    }
}