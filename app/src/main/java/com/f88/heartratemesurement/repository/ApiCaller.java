package com.f88.heartratemesurement.repository;

import android.util.Log;

import com.f88.heartratemesurement.services.ApiResponse;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCaller<T> {

    public static final String LOG = "APICaller";

    public void callApi(Call<ApiResponse<T>> call, DataCallback<T> dataCallback) {
        call.enqueue(new Callback<ApiResponse<T>>() {
            @Override
            public void onResponse(@NotNull Call<ApiResponse<T>> call, @NotNull Response<ApiResponse<T>> response) {
                if (response.body() != null) {
                    switch (response.body().getCode()) {
                        case 200: {
                            Log.i(LOG, "Api call success");
                            dataCallback.onResponse(response.body().getData());
                        }
                        break;
                        default: {
                            Log.i(LOG, "Api fail");
                            Log.i(LOG, response.body().toString());
                            dataCallback.onFailure(response.body().getMessage());
                        }
                    }
                }
                //When bad request response will have errorBody not body
                else {
                    try {
                        Log.i(ApiCaller.LOG, response.errorBody().string());
                        dataCallback.onFailure(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ApiResponse<T>> call, @NotNull Throwable t) {
                Log.i(LOG, t.getMessage());
                dataCallback.onFailure((t.getMessage()));
            }
        });
    }


}
