package com.f88.heartratemesurement.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.f88.heartratemesurement.model.Login;
import com.f88.heartratemesurement.model.LoginPost;
import com.f88.heartratemesurement.model.Register;
import com.f88.heartratemesurement.model.RegisterPost;
import com.f88.heartratemesurement.model.User;
import com.f88.heartratemesurement.model.UserPost;
import com.f88.heartratemesurement.services.ApiResponse;
import com.f88.heartratemesurement.services.RetrofitClient;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {
    private static final String TAG = "UserAPI";
    private final MutableLiveData<List<User>> users = new MutableLiveData<>();

    public MutableLiveData<List<User>> getUsers() {
        Call<ApiResponse<List<User>>> call = RetrofitClient.getInstance().getUserService().getUsers();
        call.enqueue(new Callback<ApiResponse<List<User>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<User>>> call, Response<ApiResponse<List<User>>> response) {
                if (response.body() != null) {
                    users.setValue(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<User>>> call, Throwable t) {
                Log.i(TAG, t.getMessage());
                users.setValue(null);
                call.cancel();
            }
        });
        return users;
    }

    //todo
    public void getUser(long userID, DataCallback<User> dataCallback) {
        Call<ApiResponse<User>> call = RetrofitClient.getInstance().getUserService().getUserByID("", userID);
        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                if (response.body() != null) {
                    dataCallback.onResponse(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                dataCallback.onFailure(t.getMessage());
            }
        });
    }

    public void createUser(UserPost userPost, DataCallback<User> dataCallback) {
        Call<ApiResponse<User>> call = RetrofitClient.getInstance().getUserService().createUser(userPost);
        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                if (response.body() != null) {
                    dataCallback.onResponse(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                dataCallback.onFailure(t.getMessage());
            }
        });
    }

    public void login(LoginPost loginPost, DataCallback<Login> dataCallback) {
        Call<ApiResponse<Login>> call = RetrofitClient.getInstance().getUserService().login(loginPost);

        ApiCaller<Login> apiCaller = new ApiCaller<Login>();
        apiCaller.callApi(call, dataCallback);
    }

    public void renewToken(String accessToken, String refreshToken, DataCallback<String> dataCallback) {
        Call<ApiResponse<String>> call = RetrofitClient.getInstance().getUserService().getRefreshToken(accessToken, refreshToken);

        ApiCaller<String> apiCaller = new ApiCaller<String>();
        apiCaller.callApi(call, dataCallback);
    }

    public void register(RegisterPost registerPost, DataCallback<Register> dataCallback) {
        Call<ApiResponse<Register>> call = RetrofitClient.getInstance().getUserService().register(registerPost);

        Gson gson = new Gson();
        String json = gson.toJson(registerPost);
        Log.i(ApiCaller.LOG, json);
        ApiCaller<Register> apiCaller = new ApiCaller<Register>();
        apiCaller.callApi(call, dataCallback);
    }


}
