package com.f88.heartratemesurement.repository;

import com.f88.heartratemesurement.model.History;
import com.f88.heartratemesurement.model.HistoryPost;
import com.f88.heartratemesurement.services.ApiResponse;
import com.f88.heartratemesurement.services.RetrofitClient;

import java.util.List;

import retrofit2.Call;

public class HistoryRepository {
    private static final String TAG = "History API";
    private static final long userID = 1;

    public HistoryRepository() {

    }

    public void getHistories(long userID, String accessToken, DataCallback<List<History>> dataDataCallback) {
        Call<ApiResponse<List<History>>> call = RetrofitClient.getInstance().getHistoryService().getHistories(userID, "Bearer " + accessToken);
        ApiCaller<List<History>> apiCaller = new ApiCaller<>();
        apiCaller.callApi(call, dataDataCallback);
    }

    //todo
    public void createHistory(long userID, String accessToken, HistoryPost historyPost, DataCallback<History> dataCallback) {
        Call<ApiResponse<History>> call = RetrofitClient.getInstance().getHistoryService().createHistory(userID, "Bearer " + accessToken, historyPost);
        ApiCaller<History> apiCaller = new ApiCaller<>();
        apiCaller.callApi(call, dataCallback);
    }


}
