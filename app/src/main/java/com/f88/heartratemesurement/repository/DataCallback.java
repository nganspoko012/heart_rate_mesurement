package com.f88.heartratemesurement.repository;

public interface DataCallback<T> {
    void onResponse(T data);

    void onFailure(String msg);
}
