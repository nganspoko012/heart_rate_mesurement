package com.f88.heartratemesurement.viewmodel;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class MeasurementViewModel extends AndroidViewModel {
    public MutableLiveData<Integer> bpmResult = new MutableLiveData<>();
    public static final int TIME_MEASURING = 20;
    private boolean isProgressStarted = false;
    public MutableLiveData<Integer> currentProgress = new MutableLiveData<>();
    public MutableLiveData<String> status = new MutableLiveData<>();

    public MeasurementViewModel(@NonNull Application application) {
        super(application);
        bpmResult.setValue(0);
        status.setValue("Tap here to start");
    }


    //Just a sample for Thread and synchronize
    public void startProgress(View view) {
        if (isProgressStarted)
            return;
        isProgressStarted = true;
        status.setValue("Measuring...");
        Thread thread = new Thread(() -> {
            Activity activity = (Activity) view.getContext();
            for (int i = 0; i <= TIME_MEASURING; i++) {
                int finalI = i;
                activity.runOnUiThread(() -> {
                    bpmResult.setValue(finalI * 5);
                    if (bpmResult.getValue() >= 100) {
                        isProgressStarted = false;
                        status.setValue("Finished");
                        Log.i("Progress bar", Boolean.toString(isProgressStarted));
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        );
        thread.start();
    }

}
