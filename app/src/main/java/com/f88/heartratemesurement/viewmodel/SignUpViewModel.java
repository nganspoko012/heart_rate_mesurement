package com.f88.heartratemesurement.viewmodel;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.f88.heartratemesurement.model.Register;
import com.f88.heartratemesurement.model.RegisterPost;
import com.f88.heartratemesurement.repository.DataCallback;
import com.f88.heartratemesurement.repository.UserRepository;
import com.f88.heartratemesurement.utils.Validator;

public class SignUpViewModel extends AndroidViewModel {
    private final UserRepository userRepository;
    public MutableLiveData<String> username = new MutableLiveData<>();
    public MutableLiveData<String> email = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<String> confirmPassword = new MutableLiveData<>();
    public MutableLiveData<Register> registeredUser = new MutableLiveData<>();

    public SignUpViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
    }

    private boolean checkConfirmPassword() {
        return confirmPassword.getValue().equals(password.getValue());
    }

    private boolean checkValidInput() {
        if (email.getValue() == null
                || password.getValue() == null
                || confirmPassword.getValue() == null)
            return false;
        else if (email.getValue().isEmpty() || password.getValue().isEmpty() || confirmPassword.getValue().isEmpty()) {
            return false;
        }
        return checkConfirmPassword();
    }

    private String parseUsernameFromEmail(String email) {
        String[] results = email.split("@");
        if (results.length > 0)
            return results[0];
        return email;
    }

    public void registerUser() {
        if (!Validator.isValidEmail((email.getValue() == null ? "" : email.getValue())))
        {
            Toast.makeText(getApplication(), "Invalid Email!", Toast.LENGTH_LONG).show();
        }
        else if (!checkValidInput()) {
            Toast.makeText(getApplication(), "Confirm password not match!!", Toast.LENGTH_LONG).show();
            Log.i("Register", password.getValue() + " : " + confirmPassword.getValue());
            return;
        }

        RegisterPost registerPost = new RegisterPost(parseUsernameFromEmail(email.getValue() == null ? "Default user" : email.getValue())
                , email.getValue()
                , password.getValue());
        userRepository.register(registerPost, new DataCallback<Register>() {
            @Override
            public void onResponse(Register data) {
                registeredUser.setValue(data);
                Log.i("Register", "Register success");
            }

            @Override
            public void onFailure(String msg) {
                registeredUser.setValue(null);
                Log.i("Register", "Register failed");
                Toast.makeText(getApplication(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }


}
