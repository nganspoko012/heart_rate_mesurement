package com.f88.heartratemesurement.viewmodel;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.f88.heartratemesurement.model.User;
import com.f88.heartratemesurement.model.UserPost;
import com.f88.heartratemesurement.repository.DataCallback;
import com.f88.heartratemesurement.repository.UserRepository;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    private final UserRepository userRepository;
    private final MutableLiveData<List<User>> users;
    private User user;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
        users = userRepository.getUsers();
        userRepository.getUser(2, new DataCallback<User>() {
            @Override
            public void onResponse(User data) {
                user = data;
            }

            @Override
            public void onFailure(String msg) {

            }
        });
    }

    public void createUser() {
        UserPost userPost = new UserPost("testPost", "1234", "testPost@gmail.com");
        userRepository.createUser(userPost, new DataCallback<User>() {
            @Override
            public void onResponse(User data) {
                user = data;
            }

            @Override
            public void onFailure(String msg) {
                Toast.makeText(getApplication(), "Cannot create user", Toast.LENGTH_LONG).show();
                Log.i("UserAPI", msg);
            }
        });
    }

    public MutableLiveData<List<User>> getUsers() {
        return users;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
