package com.f88.heartratemesurement.viewmodel;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.f88.heartratemesurement.model.Login;
import com.f88.heartratemesurement.model.LoginPost;
import com.f88.heartratemesurement.repository.DataCallback;
import com.f88.heartratemesurement.repository.UserRepository;


public class LoginViewModel extends AndroidViewModel {
    private final UserRepository userRepository;
    public MutableLiveData<String> email = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<Login> loggedUser = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
    }

    public void login(LoginPost loginPost) {
        userRepository.login(loginPost, new DataCallback<Login>() {
            @Override
            public void onResponse(Login data) {

                Log.i("Login", "Login success");
                loggedUser.setValue(data);
            }

            @Override
            public void onFailure(String msg) {
                loggedUser.setValue(null);
                Log.i("Login", "Login fail");
                Toast.makeText(getApplication(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

}
