package com.f88.heartratemesurement.viewmodel;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.f88.heartratemesurement.model.History;
import com.f88.heartratemesurement.model.HistoryPost;
import com.f88.heartratemesurement.repository.DataCallback;
import com.f88.heartratemesurement.repository.HistoryRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

//todo Change history with JWT

public class HistoryViewModel extends AndroidViewModel {
    private final MutableLiveData<List<History>> histories = new MutableLiveData<>();
    private final HistoryRepository repository;
    public MutableLiveData<History> lastedHistory = new MutableLiveData<>();
    SimpleDateFormat simpleDateFormat;

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        repository = new HistoryRepository();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
    }


    public void getHistoriesFromRepository(long userID, String accessToken) {
        repository.getHistories(userID, accessToken, new DataCallback<List<History>>() {
            @Override
            public void onResponse(List<History> data) {
                lastedHistory.setValue(getLastedHistory(data));
                histories.setValue(data);
            }

            @Override
            public void onFailure(String msg) {
                Toast.makeText(getApplication(), "Cannot get histories", Toast.LENGTH_LONG).show();
            }
        });
    }

    private List<History> addHistoryToLiveData(List<History> historyList, History history) {
        if (historyList == null)
            historyList = new ArrayList<History>();
        historyList.add(history);
        return historyList;
    }

    private History getLastedHistory(List<History> historyList) {
        if (historyList != null && !historyList.isEmpty()) {
            return historyList.get(0);
        }
        return null;
    }

    private List<History> sortHistories() {
        List<History> historyList = histories.getValue();
        if (historyList != null) {
            Collections.reverse(historyList);
        }
        return historyList;
    }


    public MutableLiveData<List<History>> getHistories() {
        return histories;
    }

    public void createHistory(long userID, String accessToken, HistoryPost historyPost) {
        repository.createHistory(userID, accessToken, historyPost,
                new DataCallback<History>() {
                    @Override
                    public void onResponse(History history) {
                        histories.setValue(addHistoryToLiveData(histories.getValue(), history));
                        lastedHistory.setValue(history);
                    }

                    @Override
                    public void onFailure(String msg) {
                        Log.i("HistoryAPI", msg);
                        Toast.makeText(getApplication(), "Can not create a history", Toast.LENGTH_LONG).show();
                    }
                });
    }


    public String getFormattedHistory() {
        return String.format(Locale.US, "Lasted result: %d BPM %s", lastedHistory.getValue().getValue(), simpleDateFormat.format(lastedHistory.getValue().getCreatedAt()));
    }

}
