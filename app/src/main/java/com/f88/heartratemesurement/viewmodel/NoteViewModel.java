package com.f88.heartratemesurement.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class NoteViewModel extends ViewModel {
    public MutableLiveData<String> note = new MutableLiveData<>();
}
