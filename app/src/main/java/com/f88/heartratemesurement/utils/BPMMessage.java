package com.f88.heartratemesurement.utils;

public class BPMMessage {
    private int value;
    private int progress;

    public BPMMessage(int value, int progress) {
        setValue(value);
        setProgress(progress);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = Math.max(0, value);
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = Math.max(0, progress);
    }
}
