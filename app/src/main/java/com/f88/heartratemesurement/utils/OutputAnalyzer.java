package com.f88.heartratemesurement.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.view.TextureView;

import com.f88.heartratemesurement.fragment.MeasurementFragment;

import java.util.concurrent.CopyOnWriteArrayList;

public class OutputAnalyzer {
    private final Activity activity;

    private final ChartDrawer chartDrawer;
    private final int measurementInterval = 45;
    private final int measurementLength = 15000; // ensure the number of data points is the power of two
    private final int clipLength = 3500;
    private final CopyOnWriteArrayList<Long> valleys = new CopyOnWriteArrayList<>();
    private final Handler mainHandler;
    private MeasureStore store;
    private int detectedValleys = 0;
    private int ticksPassed = 0;
    private CountDownTimer timer;
    private final boolean isMeasuring = false;

    public OutputAnalyzer(Activity activity, TextureView graphTextureView, Handler mainHandler) {
        this.activity = activity;
        this.chartDrawer = new ChartDrawer(graphTextureView);
        this.mainHandler = mainHandler;
    }

    private boolean detectValley() {
        final int valleyDetectionWindowSize = 13;
        CopyOnWriteArrayList<Measurement<Integer>> subList = store.getLastStdValues(valleyDetectionWindowSize);
        if (subList.size() < valleyDetectionWindowSize) {
            return false;
        } else {
            Integer referenceValue = subList.get((int) Math.ceil(valleyDetectionWindowSize / 2f)).measurement;

            for (Measurement<Integer> measurement : subList) {
                if (measurement.measurement < referenceValue) return false;
            }

            // filter out consecutive measurements due to too high measurement rate
            return (!subList.get((int) Math.ceil(valleyDetectionWindowSize / 2f)).measurement.equals(
                    subList.get((int) Math.ceil(valleyDetectionWindowSize / 2f) - 1).measurement));
        }
    }

    public void measurePulse(TextureView textureView, CameraService cameraService) {
        // 20 times a second, get the amount of red on the picture.
        // detect local minimums, calculate pulse.

        store = new MeasureStore();

        detectedValleys = 0;

        timer = new CountDownTimer(measurementLength, measurementInterval) {

            @Override
            public void onTick(long millisUntilFinished) {
                float progressLeft = (1 - (float) millisUntilFinished / measurementLength) * 100;
                // skip the first measurements, which are broken by exposure metering
                if (clipLength > (++ticksPassed * measurementInterval)) {
                    //Update progress bar
                    sendMessage(MeasurementFragment.MESSAGE_UPDATE_REALTIME, new BPMMessage(
                            0,
                            Math.round(progressLeft)
                    ));
                    return;
                }

                Thread thread = new Thread(() -> {
                    Bitmap currentBitmap = textureView.getBitmap();
                    int measurement = getRedComponent(currentBitmap, textureView.getWidth(), textureView.getHeight());
                    store.add(measurement);

                    if (detectValley()) {
                        detectedValleys = detectedValleys + 1;
                        valleys.add(store.getLastTimestamp().getTime());
                        // in 13 seconds (13000 milliseconds), I expect 15 valleys. that would be a pulse of 15 / 130000 * 60 * 1000 = 69
                        float currentBPM = (valleys.size() == 1)
                                ? (60f * (detectedValleys) / (Math.max(1, (measurementLength - millisUntilFinished - clipLength) / 1000f)))
                                : (60f * (detectedValleys - 1) / (Math.max(1, (valleys.get(valleys.size() - 1) - valleys.get(0)) / 1000f)));


                        sendMessage(MeasurementFragment.MESSAGE_UPDATE_REALTIME, new BPMMessage(
                                Math.round(currentBPM),
                                Math.round(progressLeft)
                        ));
                    }

                    // draw the chart on a separate thread.
                    Thread chartDrawerThread = new Thread(() -> chartDrawer.draw(store.getStdValues()));
                    chartDrawerThread.start();
                });
                thread.start();
            }

            @Override
            public void onFinish() {
                CopyOnWriteArrayList<Measurement<Float>> stdValues = store.getStdValues();


                float resultBPM = 60f * (detectedValleys - 1) / (Math.max(1, (valleys.get(valleys.size() - 1) - valleys.get(0)) / 1000f));

                sendMessage(MeasurementFragment.MESSAGE_UPDATE_FINAL, Math.round(resultBPM));

                cameraService.stop();
            }
        };
        sendMessage(MeasurementFragment.MESSAGE_START_MEASURE, "");
        timer.start();
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
    }

    void sendMessage(int what, Object message) {
        Message msg = new Message();
        msg.what = what;
        msg.obj = message;
        mainHandler.sendMessage(msg);
    }

    public int getRedComponent(Bitmap bitmap, int width, int height) {
        int count = 0;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                count += Color.red(bitmap.getPixel(x, y));
            }
        }
        return count;
    }
}
