package com.f88.heartratemesurement;

import com.f88.heartratemesurement.utils.Validator;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class EmailValidatorTest {
    @Test
    public void emailValidator_CorrectEmail() {
        assertThat(Validator.isValidEmail("test@gmail.com")).isTrue();
    }

    @Test
    public void emailValidator_NoDomainEmail() {
        assertThat(Validator.isValidEmail("test"));
    }
}
